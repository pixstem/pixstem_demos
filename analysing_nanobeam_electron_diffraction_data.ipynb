{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Analysing nanobeam electron diffraction data\n",
    "\n",
    "The analysis of this type of data in pixStem is based upon finding the position of diffraction disks, and using that for further analysis. This includes various pre-processing functions, for template matching the images with a binary disk and remove the background in the diffraction patterns. Lasty, the intensity from all the diffraction spots can be extracted.\n",
    "\n",
    "It does not assume there are a limited number of structures or orientations within the dataset. All functions can be run \"lazyily\", meaning all the data does not have to be loaded into memory at the same time. This make is possible to process data which is much larger than the available computer memory. Secondly, all the functions can be run in a parallel fashion over several cores. So more CPUs: faster processing.\n",
    "\n",
    "For more extensive analysis of this type of data, especially acquired using precession, see pyXem: https://pyxem.github.io/pyxem-website/, which has more functions for extracting information about the  crystal structure."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "* Initial version: made by Magnus Nord, 2020/2/16\n",
    "\n",
    "Requires pixStem 0.4.0 or later."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Imports and signal\n",
    "\n",
    "Firstly we set the matplotlib plotting library and pixStem."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": [
     "nbval-skip"
    ]
   },
   "outputs": [],
   "source": [
    "%matplotlib notebook"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import pixstem.api as ps"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The signal we're going to look at resembles a nanobeam electron diffraction pattern, acquired with a fast pixelated direct electron detector in scanning transmission electron microscopy mode. The convergence angle of the beam is sufficiently low, to avoid overlap between the diffraction disks.\n",
    "\n",
    "Within the datasets, the crystal structure is the same, but contains some grains which are rotated along the axis parallel to the electron beam."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "s = ps.dummy_data.get_nanobeam_electron_diffraction_signal()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "s.plot()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Finding the position of the diffraction disks\n",
    "\n",
    "The simplest way to locate the diffraction disks is to run a peak finding algorithm, without running any pre-processing, using the `find_peaks` function in the PixelatedSTEM class.\n",
    "\n",
    "This method can be run with **difference of Gaussians (DoG)** or **Laplacian of Gaussian (LoG)** peak finding routines, and has many parameters which can (and most often should) be tuned for your specific datasets. For a full list of these parameters, see the docstring (write `s.find_peaks?` in a cell, and run it). For more information about how the parameters affect the peak finding, see skimage's [Difference of Gaussian](https://scikit-image.org/docs/dev/api/skimage.feature.html#skimage.feature.blob_dog) and [Laplacian of Gaussian](https://scikit-image.org/docs/dev/api/skimage.feature.html#skimage.feature.blob_log) docstrings."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "peak_array = s.find_peaks(lazy_result=False)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This returns a NumPy array (or dask array if `lazy_result=True`) with the probe position in the first two dimensions. To access the peaks positions for a specific probe position, the `peak_array` can be sliced."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "peak_array[5, 2]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "It has only found one peak, the centre disk, since it is the most intense feature in the diffraction images! This is common in datasets such as these. It is possible to tune the peak finding parameters to find more of the disks, however this is often a tedious process. Especially for more tricky datasets, with for example randomly oriented diffraction patterns, where the intensity and disk shapes can vary a great deal.\n",
    "\n",
    "(If you want to try to find more peaks in the dataset by tuning the parameters, the threshold is a good place to start. For example by starting with a value of `threshold=0.05`. However, in more complicated datasets this can also lead to an increase in false positives.)\n",
    "\n",
    "Another way of finding the disks is doing some pre-processing of the diffraction images, by utilizing the shape of the diffraction disks."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Template match with binary disk"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "One advantage with acquiring the data with a convergent beam, is that the diffraction spots become disks. These disks are easy to separate from the wide range of others features in the diffraction images, like cosmic rays or other types of noise.\n",
    "\n",
    "A fairly easy way of finding these disks is by cross-correlating each diffraction image with a binary disk: `s.template_match_disk`. The only input parameter is the radius of this disk, `disk_r`. For this dataset, lets set `disk_r=5`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "st = s.template_match_disk(disk_r=5, lazy_result=False)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This returns a new dataset, with the same size as the original dataset."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "st.plot()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Here, the disks are much more visible, and we can apply the peak finding directly on this template matched dataset."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "peak_array = st.find_peaks(lazy_result=False)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To visualize them on the template matched signal, we use `add_peak_array_as_markers`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "st.add_peak_array_as_markers(peak_array)\n",
    "st.plot()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This `peak_array` can also be added to the original signal, to see how well the peak finding worked. To delete the markers, run: `del s.metadata.Markers`"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "s.add_peak_array_as_markers(peak_array)\n",
    "s.plot()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This seems to have worked pretty well! Next, we can refine each peak position using centre of mass."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Refining the peak positions"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "peak_array_com = s.peak_position_refinement_com(peak_array, lazy_result=False)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We compare before and after by using a different color for the `peak_array_com`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "s.add_peak_array_as_markers(peak_array_com, color='blue')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "s.plot()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This had some effect, but especially towards the very intense centre part of the diffraction image, the peaks are obviously shifted due to the background."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Removing the background"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "There are several methods for removing the background, with a range of parameters: `difference of gaussians`, `median kernel` and `radial median`. Lets go with the default: `median kernel`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": [
     "nbval-skip"
    ]
   },
   "outputs": [],
   "source": [
    "s_rem = s.subtract_diffraction_background(lazy_result=False)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": [
     "nbval-skip"
    ]
   },
   "outputs": [],
   "source": [
    "s_rem.plot()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Then we can apply the same center of mass refinement."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": [
     "nbval-skip"
    ]
   },
   "outputs": [],
   "source": [
    "peak_array_rem_com = s_rem.peak_position_refinement_com(peak_array, lazy_result=False)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "And plot this on the signal where background has been removed."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": [
     "nbval-skip"
    ]
   },
   "outputs": [],
   "source": [
    "s_rem.add_peak_array_as_markers(peak_array_rem_com)\n",
    "s_rem.plot()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Getting the intensity\n",
    "\n",
    "Lastly, we can extract the intensity from each of the diffraction spots."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": [
     "nbval-skip"
    ]
   },
   "outputs": [],
   "source": [
    "peak_array_intensity_rem = s_rem.intensity_peaks(peak_array_rem_com, lazy_result=False)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This returns a NumPy array similar to the one we've seen earlier, but with an extra column. To extract a single peak from a single position:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": [
     "nbval-skip"
    ]
   },
   "outputs": [],
   "source": [
    "peak_array_intensity_rem[5, 2][10]"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.6"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
