{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Basic structural analysis of 4D-STEM data\n",
    "\n",
    "This notebook shows how to use the `pixstem` library to analyse 4D scanning transmission electron microscopy (STEM) data. Here, we're looking at a dataset from a perovskite oxide thin film sample.\n",
    "\n",
    "More documentation for the data processing routines in pixStem is found at https://pixstem.org\n",
    "\n",
    "Journal article about the dataset:\n",
    "* **Three-dimensional subnanoscale imaging of unit cell doubling due to octahedral tilting and cation modulation in strained perovskite thin films**\n",
    "* Magnus Nord, Andrew Ross, Damien McGrouther, Juri Barthel, Magnus Moreau, Ingrid Hallsteinsen, Thomas Tybell, and Ian MacLaren\n",
    "* Phys. Rev. Materials 3, 063605 – Published 14 June 2019\n",
    "* https://doi.org/10.1103/PhysRevMaterials.3.063605\n",
    "\n",
    "Dataset and scripts used in processing the data for the paper:\n",
    "* Zenodo DOI: https://dx.doi.org/10.5281/zenodo.3476746\n",
    "* The actual dataset used here, which is a rechunked version of the dataset used in the paper: https://zenodo.org/record/3687144. The original dataset had suboptimal chunking, which lead to very slow data processing"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Importing libraries"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The first step is setting the plotting toolkit"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": [
     "nbval-skip"
    ]
   },
   "outputs": [],
   "source": [
    "%matplotlib qt5"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Then import the library itself"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import pixstem.api as ps"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Working with fast pixelated detector STEM data"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Downloading the data from Zenodo"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": [
     "nbval-skip"
    ]
   },
   "outputs": [],
   "source": [
    "import os.path\n",
    "if not os.path.exists('m004_LSMO_LFO_STO_medipix_rechunked.hspy'):\n",
    "    import urllib.request\n",
    "    urllib.request.urlretrieve('https://zenodo.org/record/3687144/files/m004_LSMO_LFO_STO_medipix_rechunked.hspy', 'm004_LSMO_LFO_STO_medipix_rechunked.hspy')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Loading data"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "s = ps.load_ps_signal(\"m004_LSMO_LFO_STO_medipix_rechunked.hspy\", lazy=True)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This returns a `LazyPixelatedSTEM` class, which is inherits HyperSpy's `Signal2D`. So all functions which work in `Signal2D`, also works here."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Plotting"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Plotting the signal directly will be very slow, as the navigation figure has to be generated. A quicker way is using `navigator='slider'`, where the navigation figure is not made."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "s.plot(navigator='slider')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Virtual detectors"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The `virtual_annular_dark_field` is used to construct a images from the `PixelatedSTEM` class, with the input being `(x, y, r_outer, r_inner)`"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "s_adf = s.virtual_annular_dark_field(128, 128, 70, 128)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "s_adf.plot()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This signal can be used as a navigator for the original dataset"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "s.plot(navigator=s_adf)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "There is also a virtual bright field method. Passing no parameters to the method gives a sum of the diffraction dimensions:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "s_bf = s.virtual_bright_field()\n",
    "s_bf.plot()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "A mask can be applied in the form of (x, y, r):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "s_bf = s.virtual_bright_field(128, 128, 30)\n",
    "s_bf.plot()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Radial averaging"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "A common task is getting the intensity as a function of scattering angle. This is done using radial integration, which firstly requires finding the center of the electron beam. Here we use the `center_of_mass` function."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "s_com = s.center_of_mass(threshold=1.)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This returns a `DPCSignal2D` class, which will be explored more later. What we need to know is that is it basically a HyperSpy `Signal2D` class, where the x-beam shifts are in the first navigation index (`s.inav[0]`), while the y-shifts are in the second navigation index (`s.inav[1]`)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "s_com.plot()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To do the radial integration itself, use the `radial_integration` method, which requires the `centre_x` and `centre_y` arguments to be specified."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "s_radial = s.radial_average(centre_x=s_com.inav[0].data, centre_y=s_com.inav[1].data)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This returns a new signal, where the signal dimensions has been reduced from 2 to 1 dimensions. This is especially useful when working with large datasets, where this operation can drastically reduce the data size, making it possible to load the full data into memory."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "s_radial"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Plotting it shows the electron scattering for each probe position:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "s_radial.plot()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To rather visualize the data as function of scattering angle (essentially virtual annular dark field), we can transpose the data using `s_radial.T`. This \"flips\" the signal and navigation axes:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "s_radial.T.plot()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Move the navigator to around 50 on the x-axis, to see the intensity distribution of the HOLZ ring.\n",
    "\n",
    "This can also be extracted by slicing the radial signal."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "s_holz = s_radial.isig[48:52].mean(-1).T"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "s_holz.plot()"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.6"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
